package com.company.config;


import javax.sql.DataSource;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

@Configuration
public class Config {

		@Bean
	    public DataSource mysqlDataSource() {
	        DriverManagerDataSource dataSource = new DriverManagerDataSource();
	        dataSource.setDriverClassName("com.mysql.jdbc.Driver");
	        dataSource.setUrl("jdbc:mysql://localhost:3306/springjdbc");
	        dataSource.setUsername("root");
	        dataSource.setPassword("serkantas123");
	 
	        return dataSource;
	    }
		
		@Bean
		public JdbcTemplate jTemplate(DataSource dataSource) {
			return 	new JdbcTemplate(dataSource);
		}
}
