package com.company.controller;

import javax.inject.Inject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.company.dao.UserDao;
import com.company.model.User;

@Controller
public class UserController {

		

	
	@RequestMapping(value="/form",method=RequestMethod.GET)
	public String index(Model model)
	{
		model.addAttribute(new User());
		return "user";
	}
	
	@RequestMapping(value="/save",method=RequestMethod.POST)
	public String save(User user)
	{
		UserDao dao = new UserDao();
		dao.create(user);
		return "user";
	}
}
