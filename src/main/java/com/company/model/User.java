package com.company.model;

import javax.validation.constraints.NotNull;
/**
 * 
 * User pojo class
 * 
 * */
public class User {

	
	private String name;
	
	
	private long tcNo;
	
	
	private String address;
	
	private String tel;
	
	
	private String email;

	private String birtDay;
	
	private boolean isRetailTrade;
	
	
	private String chooseLokumcuBaba;
	
	private String thingAdress;
	
	private int investmentPrice;
	
	private String note;
	
	public User() {
		// TODO Auto-generated constructor stub
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public long getTcno() {
		return tcNo;
	}
	public void setTcno(long tcno) {
		this.tcNo = tcno;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getTel() {
		return tel;
	}
	public void setTel(String tel) {
		this.tel = tel;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getBirtDay() {
		return birtDay;
	}
	public void setBirtDay(String birtDay) {
		this.birtDay = birtDay;
	}
	public boolean isRetailTrade() {
		return isRetailTrade;
	}
	public void setRetailTrade(boolean isRetailTrade) {
		this.isRetailTrade = isRetailTrade;
	}
	public String getChooseLokumcuBaba() {
		return chooseLokumcuBaba;
	}
	public void setChooseLokumcuBaba(String chooseLokumcuBaba) {
		this.chooseLokumcuBaba = chooseLokumcuBaba;
	}
	public String getThingAdress() {
		return thingAdress;
	}
	public void setThingAdress(String thingAdress) {
		this.thingAdress = thingAdress;
	}
	public int getInvestmentPrice() {
		return investmentPrice;
	}
	public void setInvestmentPrice(int investmentPrice) {
		this.investmentPrice = investmentPrice;
	}
	public String getNote() {
		return note;
	}
	public void setNote(String note) {
		this.note = note;
	}
	
}
