<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<link rel="stylesheet" type="text/css" media="screen" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css">
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
<script src="//netdna.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
<link href="//cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/e8bddc60e73c1ec2475f827be36e1957af72e2ea/build/css/bootstrap-datetimepicker.css" rel="stylesheet"><script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/js/bootstrap-datepicker.js"></script>


<style>

.red{
    color:red;
    }
.form-area
{
    background-color: #FAFAFA;
	padding: 10px 40px 60px;
	margin: 10px 0px 60px;
	border: 1px solid GREY;
	}
</style>

</head>
<body>

<div class="container">
<div class="col-md-5">
    <div class="form-area">  
        <form  action="/controller/save" commandName="user" method="post">
        <br style="clear:both">
                    <h3 style="margin-bottom: 25px; text-align: center;">BAYİLİK ÖN BAŞVURU FORMU</h3>
    				<div class="form-group">
						<input type="text" class="form-control" id="name" name="name" placeholder="Ad Soyad" required>
					</div>
					<div class="form-group">
						<input type="number" class="form-control" id="tc" name="tcNo" placeholder="Tc Kimlik" required>
					</div>
					<div class="form-group">
						<input type="email" class="form-control" name="email" placeholder="Email" required>
					</div>
					 <div class="form-group">
                <div class='input-group date' id='datetimepicker1'>
                    <input type='text' class="form-control" name="birtDay"/>
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                </div>
            </div>
					<div class="form-group">
						<input type="text" class="form-control" id="adress" name="address" placeholder="Adres" required>
					</div>
					<div class="form-group">
					<br>
					 	<h5>PERAKENDE TİCARETİ İLE UĞRAŞTINIZ MI? </h5>
						<input type="radio"  name="isRetailTrade" value="1" checked="checked">Evet
						<input type="radio"  name="isRetailTrade" value="0">Hayır
					</div>
					
					<div class="form-group">
					 	<h5>LOKUMCU BABA’YI TERCİH ETMENİZİN SEBEBİ NEDİR? </h5>
					 	<select class="form-control" name="chooseLokumcuBaba">
					 		<option>Seçenek1</option>
					 		<option>Seçenek2</option>
					 		<option>Seçenek3</option>
					 	
					 	</select>
					</div>
				
					<div class="form-group">
						<input type="text" class="form-control" name="thingAdress" placeholder="Hangi İl/İlçe/Semt için düşünüyorsunuz? " required>
					</div>
					<div class="form-group">
						<input type="number" class="form-control"  name="investmentPrice" placeholder="Yatırım Miktarınız Nedir? " required>
					</div>
                    <div class="form-group">
                    <textarea class="form-control" type="textarea" name="note" placeholder="EKLEMEK İSTEDİKLERİNİZ" maxlength="140" rows="7"></textarea>
                        <span class="help-block"><p id="characterLeft" class="help-block ">You have reached the limit</p></span>                    
                    </div>
            
        <button type="submit" id="submit" name="submit" class="btn btn-primary pull-right">Gönder</button>
        </form>
    </div>
</div>
</div>
<script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment-with-locales.js"></script>
<script src="//cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/e8bddc60e73c1ec2475f827be36e1957af72e2ea/src/js/bootstrap-datetimepicker.js"></script>
<script type="text/javascript">
$(document).ready(function(){ 
    $('#characterLeft').text('140 karakter');
    $('#message').keydown(function () {
        var max = 140;
        var len = $(this).val().length;
        if (len >= max) {
            $('#characterLeft').text('Karakter limitiniz bitmiştir');
            $('#characterLeft').addClass('red');
            $('#btnSubmit').addClass('disabled');            
        } 
        else {
            var ch = max - len;
            $('#characterLeft').text(ch + ' karakter');
            $('#btnSubmit').removeClass('disabled');
            $('#characterLeft').removeClass('red');            
        }
    });
       
    $('#datetimepicker1').datepicker();

});
</script>
</body>
</html>